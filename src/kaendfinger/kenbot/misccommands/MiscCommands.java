package kaendfinger.kenbot.misccommands;

import kaendfinger.kenbot.api.IModule;
import kaendfinger.kenbot.api.events.ConfigurationEvent;
import kaendfinger.kenbot.api.registry.Command;
import org.pircbotx.hooks.events.MessageEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MiscCommands implements IModule {

    @Command
    public static void time(MessageEvent event) {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        event.getChannel().sendMessage(dateFormat.format(cal.getTime()));
    }

    @Command
    public static void make(MessageEvent event) {
        String[] fullCmd = event.getMessage().split(" ");
        String full = "";
        if (fullCmd[1].equalsIgnoreCase("me")) {
            fullCmd[1] = "you";
        }
        for (int i = 1; i <= fullCmd.length - 1; i++) {
            if (i != fullCmd.length - 1) {
                full += fullCmd[i] + " ";
            } else {
                full += fullCmd[i];
            }

        }
        event.respond("I am now making " + full);
    }

    @Override
    public void setup(ConfigurationEvent event) {
    }

    @Override
    public void load() {
    }

    @Override
    public void unload() {
    }

    public String[] classes() {
        return new String[]{
                "kaendfinger.kenbot.misccommands.MiscCommands"
        };
    }

    @Override
    public String name() {
        return "Misc Commands";
    }

}
